﻿//MIT License

//Copyright(c) 2019 Federico Gallucci

//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:

//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.

//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

using UnityEngine;
using System.IO;
using UnityEditor;
using System.Linq;

namespace safriaduo.CompressedTextureImporter
{
    /// <summary>
    /// Resize textures that aren't multiple of four
    /// </summary>
    public static class CompressedTextureImporter
    {
        [MenuItem("Compressed Texture Importer/Resize Selected Images")]
        public static void ResizeSelectedImages()
        {
            Texture2D[] textures = Selection.GetFiltered(typeof(Texture2D), SelectionMode.Assets)
                                            .Cast<Texture2D>()
                                            .ToArray();

            MakeTextureCompressable(textures);
        }

        private static void MakeTextureCompressable(Texture2D[] textures)
        {
            int width, height;

            foreach (var texture in textures)
            {
                width = GetNextPowerOfFour(texture.width);
                height = GetNextPowerOfFour(texture.height);

                ResizeTexture(texture, width, height);
            }

            AssetDatabase.SaveAssets();

            Debug.Log($"[COMPRESSED TEXTURE IMPORTER] TextureS resized successfully. Number of texture resized: {textures.Length}");
        }

        private static void ResizeTexture(Texture2D oldTex, int width, int height)
        {
            Texture2D newTex = new Texture2D(width, height);

            Color[] oldTexPixels = oldTex.GetPixels();
            Color[] newTexPixels = new Color[width * height];

            int oldWidth = oldTex.width, oldHeight = oldTex.height;

            //This is because the pixels are stored starting from the width in an array
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    if (j < oldWidth && i < oldHeight)
                        newTexPixels[i * width + j] = oldTexPixels[i * oldWidth + j];
                    else
                        newTexPixels[i * width + j] = Color.clear;
                }
            }

            newTex.SetPixels(newTexPixels);
            newTex.Apply();

            byte[] encodeToPNG = newTex.EncodeToPNG();

            string path = AssetDatabase.GetAssetPath(oldTex);

            File.WriteAllBytes(path, encodeToPNG);

            AssetDatabase.Refresh();
        }

        [MenuItem("Compressed Texture Importer/Resize All Images")]
        public static void ResizeAllImages()
        {
            Texture2D[] textures = FindAllProjectTextures();

            MakeTextureCompressable(textures);
        }

        private static Texture2D[] FindAllProjectTextures()
        {
            string[] assetFiles = AssetDatabase.FindAssets("t:Texture", new[] { "Assets" });

            Texture2D[] textures = new Texture2D[assetFiles.Length];
            Texture2D current;

            for (int i = 0; i < assetFiles.Length; i++)
            {
                current = AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(assetFiles[i]), typeof(Texture2D)) as Texture2D;
                textures[i] = current;
            }

            return textures;
        }

        private static int GetNextPowerOfFour(int number)
        {
            return Mathf.CeilToInt(number / 4f) * 4;
        }
    }
}
